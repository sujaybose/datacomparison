import os
import time
import warnings
import pyodbc

from core.Utilities.utilities import compare

warnings.filterwarnings("ignore")
beginTime = time.time()

# Specify the path
matchFile = r"C:\Projects\DataComparison\ResultFolder\matchTaxRate.txt";
unMatchFile = r"C:\Projects\DataComparison\ResultFolder\unmatchTaxRate.txt";

if os.path.exists(matchFile):
    os.remove(matchFile)
if os.path.exists(unMatchFile):
    os.remove(unMatchFile)
try:
    key = "JurisdictionId";
    recordsProcessed = 0;
    rowLimit = 50000;
    effDate = '04-01-2020'
    endDate = '03-31-2020'

    DSN_NAME = '<Target DSN Name>'
    DATA_BASE_NAME = 'TaxCalculate'
    TABLE_NAME = 'TaxRate'
    TABLE_SCHEMA = 'dbo'
    columnList = ['EffDate','EndDate','JurisdictionId','Rate','RateTypeId','TaxTypeId','TaxName','TaxRegionId','Source']
    # Generate Query for Comparision
    selectStr = "Select "
    temp = ""
    for value in columnList:
        temp = temp + "," + value

    queryStatement = selectStr + temp[1:] + ' FROM ' + \
                     DATA_BASE_NAME + '.' + TABLE_SCHEMA + '.' + TABLE_NAME +" Where (EffDate= '" + effDate + "' or EndDate= '" + endDate + "') order by " + key

    keyConnection = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL '
                                   'Server};SERVER=;DATABASE=;UID=;PWD=',
                                   autocommit=True)
    keyCursor = keyConnection.cursor()
    keyQuery = """
    SELECT COLUMN_NAME
    FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS TC
    INNER JOIN
    INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KU
    ON TC.CONSTRAINT_TYPE = 'PRIMARY KEY'
    AND TC.CONSTRAINT_NAME = KU.CONSTRAINT_NAME
    AND KU.TABLE_NAME = ?
    AND KU.TABLE_SCHEMA = ?
    AND KU.TABLE_CATALOG = ?
    """

    keyResult = keyCursor.execute(keyQuery, (TABLE_NAME, TABLE_SCHEMA, DATA_BASE_NAME))
    keyList = []
    for keyIndex, keyRow in enumerate(keyResult):
        keyList.append(keyRow[0])

    keyCursor.close()

    sourceRowQueryString = queryStatement
    print (queryStatement)
    sourceRowConnection = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL '
                                   'Server};SERVER=;DATABASE=;UID=;PWD=',
                                   autocommit=True)
    print('source db connected')
    sourceRowCursor = sourceRowConnection.cursor()

    targetRowQueryString = queryStatement
    targetRowConnection = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL '
                                         'Server};SERVER=;DATABASE=;Trusted_Connection=yes;',
                                         autocommit=True)
    print('target db connected')
    targetRowCursor = targetRowConnection.cursor()

    sourceRowCursor.execute(sourceRowQueryString)
    targetRowCursor.execute(targetRowQueryString)

    while True:
        batchTime = time.time()
        sourceRowResult = sourceRowCursor.fetchmany(rowLimit)
        if not sourceRowResult:
            break
        targetRowResult = targetRowCursor.fetchmany(rowLimit)
        if not targetRowResult:
            break

        sourceRowObjectList = []

        for i, sourceRow in enumerate(sourceRowResult):
            sourceKeyString = '';
            sourceRowObject = {}
            for j, sourceRowValue in enumerate(sourceRow):
                sourceRowObject[columnList[j]] = sourceRowValue
                if columnList[j] in keyList:
                    sourceKeyString = sourceKeyString + str(sourceRowValue)
            sourceRowObject["compareKey"] = sourceKeyString
            sourceRowObjectList.append(sourceRowObject)

        targetRowObjectList = []
        for m, targetRow in enumerate(targetRowResult):
            targetKeyString = '';
            targetRowObject = {}
            for n, targetRowValue in enumerate(targetRow):
                targetRowObject[columnList[n]] = targetRowValue
                if columnList[n] in keyList:
                    targetKeyString = targetKeyString + str(targetRowValue)
            targetRowObject["compareKey"] = targetKeyString
            targetRowObjectList.append(targetRowObject)

        compare(sourceRowObjectList, targetRowObjectList, matchFile, unMatchFile, key)
        recordsProcessed = recordsProcessed + len(targetRowObjectList)
        print(str(recordsProcessed) + ' rows compared')
        print('Batch compare time for Target [Exporter] ' + str(
            len(targetRowObjectList)) + ' rows and Source[Consolidation i.e. Extractor] ' + str(
            len(sourceRowObjectList)) + ' rows: ' + str(time.time() - batchTime));

    sourceRowCursor.close()
    # targetRowCursor.close()
    print('Total Time: ' + str(time.time() - beginTime));

except Exception as e:
    print(e)
